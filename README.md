# CoffeeFreak – Blank Magento extension for building main admin menu with sidebar and tabs • Inchoo

By: Branko Ajzele, Aug 26, 2009 

![][1]

My previous article, [CoolDash – Blank Magento extension for building admin system configuration area][2], was about blank start-up extension for building System > Configuration admin area. I used a lot of "funny" attribute names so that "get around" gets some speed. This extension is somewhat similar to previous one, except its meant to be a **blank start-up extension for building items under main admin menu with sidebar and tabs**.

Keep in mind that, once again, extension name is something I popped out of my head while I was making myself a coffee:).

Below are two screenshots for you to see final result.

![NoSidebar][3]

![SidebarInView][4]

In short, this extension demonstrates the use of controllers (router definition under config file) and widgets in Magento.

Download [Inchoo_CoffeeFreak][5] extension for Magento.

Hope you find it useful.

[1]: https://gitlab.com/magento1x/inchoo_coffeefreak/raw/master/coffeefreak.jpg "CoffeeFreak – Blank Magento extension for building main admin menu with sidebar and tabs"
[2]: http://inchoo.net/ecommerce/magento/cooldash-blank-magento-extension-for-building-admin-system-configuration-area/
[3]: https://gitlab.com/magento1x/inchoo_coffeefreak/raw/master/NoSidebar.png "NoSidebar"
[4]: https://gitlab.com/magento1x/inchoo_coffeefreak/raw/master/SidebarInView.png "SidebarInView"
[5]: https://gitlab.com/magento1x/inchoo_coffeefreak/raw/master/Inchoo_CoffeeFreak.zip